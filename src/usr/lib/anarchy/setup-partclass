#!/usr/bin/env bash
# Copyright (C) 2017 Dylan Schacht

part_class() {
  op_title="${edit_op_msg}"
  if [ -z "${part}" ]; then
    unset DRIVE ROOT
    return
  else
    part_size=$(grep <<<"${device_list}" -w "${part}" | awk '{print $2}')
    part_type=$(grep <<<"${device_list}" -w "${part}" | awk '{print $3}')
    part_fs=$(grep <<<"${device_list}" -w "${part}" | awk '{print $4}')
    part_mount=$(df | grep -w "${part}" | awk '{print $6}' | sed 's/mnt\/\?//')
  fi

  if [ "${part_type}" == "lvm" ]; then
    part=${part/-//}
  fi

  if [ "${part_fs}" == "linux_raid_member" ]; then # do nothing
    part_menu
  elif [ "${part_type}" == "disk" ] || ( (grep -E "raid[0-9]+" <<<"${part_type}" &>/dev/null) && [ -z "${part_fs}" ]); then # Partition

    source "${lang_file}"

    if (df | grep -w "${part}" | grep "${ARCH}" &>/dev/null); then
      if (yesno "\n${mount_warn_var}" "${edit}" "${cancel}" 1); then
        points=$(echo -e "${points_orig}\n${custom} ${custom}-mountpoint")
        (
          umount -R "${ARCH}"
          swapoff -a
        ) &>/dev/null &
        pid=$! pri=0.1 msg="${wait_load} \n\n \Z1> \Z2umount -R /mnt\Zn" load
        mounted=false
        unset DRIVE
        select_util
        if "${gpart}"; then
          ${UTIL} /dev/"${part}" &>/dev/null
        else
          ${UTIL} /dev/"${part}"
        fi
        sleep 0.5
        clear
      fi
    elif (yesno "\n${manual_part_var3}" "${edit}" "${cancel}"); then
      select_util
      if "${gpart}"; then
        ${UTIL} /dev/"${part}" &>/dev/null
      else
        ${UTIL} /dev/"${part}"
      fi
      sleep 0.5
      clear
    fi

    part_menu

  elif [ "${part}" == "${done_msg}" ]; then # Done

    if ! "${mounted}"; then
      msg "\n${root_err_msg1}"
      part_menu
    else
      if [ -z "${BOOT}" ]; then
        BOOT="${ROOT}"
      fi

      final_part=$( (
        df -h | grep "${ARCH}" | awk '{print $1,$2,$6 "\\n"}' | sed 's/mnt\/\?//'
        swapon | awk 'NR==2 {print $1,$3,"SWAP"}'
      ) | column -t)
      final_count=$(wc <<<"${final_part}" -l)

      if [ "${final_count}" -lt "7" ]; then
        height=17
      elif [ "${final_count}" -lt "13" ]; then
        height=23
      elif [ "${final_count}" -lt "17" ]; then
        height=26
      else
        height=30
      fi

      part_menu="${partition}: ${size}: ${mountpoint}:"

      if (yesno "\n${write_confirm_msg} \n\n ${part_menu} \n\n${final_part} \n\n ${write_confirm}" "${write}" "${cancel}" 1); then
        if (efivar -l &>/dev/null); then
          if (fdisk -l | grep "EFI" &>/dev/null); then
            if (yesno "\n${efi_man_msg}" "${yes}" "${no}"); then
              if [ "$(fdisk -l | grep -c "EFI")" -gt "1" ]; then
                efint=1
                while (true); do
                  if [ "$(fdisk -l | grep "EFI" | awk "NR==${efint} {print \$1}")" == "" ]; then
                    msg "${efi_err_msg1}"
                    part_menu
                  fi
                  esp_part=$(fdisk -l | grep "EFI" | awk "NR==${efint} {print \$1}")
                  esp_mnt=$(df -T | grep "${esp_part}" | awk '{print $7}' | sed 's|/mnt||')
                  if (df -T | grep "${esp_part}" &>/dev/null); then
                    break
                  else
                    efint=$((efint + 1))
                  fi
                done
              else
                esp_part=$(fdisk -l | grep "EFI" | awk '{print $1}')
                if ! (df -T | grep "${esp_part}" &>/dev/null); then
                  source "${lang_file}"
                  if (yesno "\n${efi_mnt_var}" "${yes}" "${no}"); then
                    if ! (mountpoint "${ARCH}"/boot &>/dev/null); then
                      mkdir "${ARCH}"/boot &>/dev/null
                      mount "${esp_part}" "${ARCH}"/boot
                    else
                      msg "\n${efi_err_msg}"
                      part_menu
                    fi
                  else
                    part_menu
                  fi
                else
                  esp_mnt=$(df -T | grep "${esp_part}" | awk '{print $7}' | sed 's|/mnt||')
                fi
              fi
              source "${lang_file}"
              if ! (df -T | grep "${esp_part}" | grep "vfat" &>/dev/null); then
                if (yesno "\n${vfat_var}" "${yes}" "${no}"); then
                  (
                    umount -R "${esp_mnt}"
                    mkfs.vfat -F32 "${esp_part}"
                    mount "${esp_part}" "${esp_mnt}"
                  ) &>/dev/null &
                  pid=$! pri=0.2 msg="\n${efi_load1} \n\n \Z1> \Z2mkfs.vfat -F32 ${esp_part}\Zn" load
                  UEFI=true
                else
                  part_menu
                fi
              else
                UEFI=true
                export esp_part esp_mnt
              fi
            fi
          fi
        fi

        if "${enable_f2fs}"; then
          if ! (df | grep "${ARCH}/boot\|${ARCH}/boot/efi" &>/dev/null); then
            FS="f2fs" source "${lang_file}"
            msg "\n${fs_err_var}"
            part_menu
          fi
        elif "${enable_btrfs}"; then
          if ! (df | grep "${ARCH}/boot\|${ARCH}/boot/efi" &>/dev/null); then
            FS="btrfs" source "${lang_file}"
            msg "\n${fs_err_var}"
            part_menu
          fi
        fi

        sleep 1
        pid=$! pri=0.1 msg="${wait_load} \n\n \Z1> \Z2Finalize...\Zn" load
        return
      else
        part_menu
      fi
    fi

  else # Install on a partition or md device with a file system
    source "${lang_file}" &>/dev/null

    if [ -z "${ROOT}" ]; then
      case "${part_size}" in
      [1-9]T | [4-9]G | [1-9][0-9]*[GT] | [4-9].*[GT] | [4-9],*[GT])
        if (yesno "\n${root_var}" "${yes}" "${no}" 1); then
          f2fs=$(lsblk -dnro ROTA /dev/${part})
          fs_select

          if [ "$?" -gt "0" ]; then
            part_menu
          fi

          source "${lang_file}"

          if (yesno "\n${root_confirm_var}" "${write}" "${cancel}" 1); then
            (
              sgdisk --zap-all /dev/"${part}"
              wipefs -a /dev/"${part}"
            ) &>/dev/null &
            pid=$! pri=0.1 msg="\n${frmt_load} \n\n \Z1> \Z2wipefs -a /dev/${part}\Zn" load

            case "${FS}" in
            jfs | reiserfs)
              echo -e "y" | mkfs."${FS}" /dev/"${part}" &>/dev/null &
              ;;
            *)
              mkfs."${FS}" /dev/"${part}" &>/dev/null &
              ;;
            esac
            pid=$! pri=1 msg="\n${load_var1} \n\n \Z1> \Z2mkfs.${FS} /dev/${part}\Zn" load

            (
              mount /dev/"${part}" "${ARCH}"
              echo "$?" >/tmp/ex_status.var
            ) &>/dev/null &
            pid=$! pri=0.1 msg="\n${mnt_load} \n\n \Z1> \Z2mount /dev/${part} ${ARCH}\Zn" load

            if [ "$(</tmp/ex_status.var)" -eq "0" ]; then
              mounted=true
              ROOT="${part}"
              if [ "${part_type}" == "lvm" ]; then
                lvm_pv=$(lvdisplay -m | grep -A 20 /dev/${part} | grep "Physical volume" | sed 's/^\s\+//g;s/\s\+/ /g' | cut -d ' ' -f 3)
                DRIVE=$(lsblk -dnro PKNAME ${lvm_pv})
              else
                DRIVE=$(lsblk -dnro PKNAME /dev/${part})
              fi
            else
              msg "\n${part_err_msg1}"
              return
            fi
          fi
        else
          part_menu
        fi
        ;;
      *)
        msg "\n${root_err_msg}"
        ;;
      esac
    elif [ -n "${part_mount}" ]; then
      if (yesno "\n${manual_part_var0}" "${edit}" "${back}" 1); then
        if [ "${part}" == "${ROOT}" ]; then
          if (yesno "\n${manual_part_var2}" "${yes}" "${no}" 1); then
            mounted=false
            unset ROOT DRIVE
            umount -R "${ARCH}" &>/dev/null &
            pid=$! pri=0.1 msg="${wait_load} \n\n \Z1> \Z2umount -R ${ARCH}\Zn" load
          fi
        else
          if [ "${part_mount}" == "[SWAP]" ]; then
            if (yesno "\n${manual_swap_var}" "${yes}" "${no}" 1); then
              swapoff /dev/"${part}" &>/dev/null &
              pid=$! pri=0.1 msg="${wait_load} \n\n \Z1> \Z2swapoff /dev/${part}\Zn" load
            fi
          elif (yesno "\n${manual_part_var1}" "${yes}" "${no}" 1); then
            umount "${ARCH}"/"${part_mount}" &>/dev/null &
            pid=$! pri=0.1 msg="${wait_load} \n\n \Z1> \Z2umount ${ARCH}${part_mount}\Zn" load
            rm -r "${ARCH}"/"${part_mount}"
            points=$(echo -e "${part_mount}   mountpoint>\n${points}")
          fi
        fi
      fi
    elif (yesno "\n${manual_new_part_var}" "${edit}" "${back}"); then
      part_swap=false
      if (fdisk -l /dev/"$(lsblk -dnro PKNAME /dev/"${part}")" | grep "gpt" &>/dev/null); then
        part_type_uuid=$(fdisk -l -o Device,Size,Type-UUID | grep -w "${part}" | awk '{print $3}')

        if [ "${part_type_uuid}" == "0657FD6D-A4AB-43C4-84E5-0933C84B4F4F" ]; then
          part_swap=true
        fi
      else
        part_type_id=$(fdisk -l | grep -w "${part}" | sed 's/\*//' | awk '{print $6}')

        if [ "${part_type_id}" == "82" ]; then
          part_swap=true
        fi
      fi

      if (${part_swap}); then
        mnt="SWAP"
      else
        mnt=$(dialog --ok-button "${ok}" --cancel-button "${cancel}" --menu "${mnt_var0}" 15 60 6 "${points}" 3>&1 1>&2 2>&3)
        if [ "$?" -gt "0" ]; then
          part_menu
        fi
      fi

      if [ "${mnt}" == "${custom}" ]; then
        while (true); do
          mnt=$(dialog --ok-button "${ok}" --cancel-button "${cancel}" --inputbox "${custom_msg}" 10 50 "/" 3>&1 1>&2 2>&3)

          if [ "$?" -gt "0" ]; then
            part_menu
            break
          elif (grep <<<"${mnt}" "[\[\$\!\'\"\`\\|%&#@()+=<>~;:?.,^{}]\|]" &>/dev/null); then
            msg "\n${custom_err_msg0}"
          elif (grep <<<"${mnt}" "^[/]$" &>/dev/null); then
            msg "\n${custom_err_msg1}"
          else
            break
          fi
        done
      fi

      if [ "${mnt}" != "SWAP" ]; then
        if (yesno "\n${part_frmt_msg}" "${yes}" "${no}" 1); then
          f2fs=$(lsblk -dnro ROTA /dev/"${part}")

          if [ "${mnt}" == "/boot/EFI" ] || [ "${mnt}" == "/boot/efi" ]; then
            f2fs=1
            btrfs=false
          fi

          if (fdisk -l | grep "${part}" | grep "EFI" &>/dev/null); then
            vfat=true
          fi

          fs_select

          if [ "$?" -gt "0" ]; then
            part_menu
          fi
          frmt=true
        else
          frmt=false
        fi

        if [ "${mnt}" == "/boot" ] || [ "${mnt}" == "/boot/EFI" ] || [ "${mnt}" == "/boot/efi" ]; then
          BOOT="${part}"
        fi
      else
        FS="SWAP"
      fi

      source "${lang_file}"

      if [ "${mnt}" == "SWAP" ]; then
        if (yesno "\n${swap_frmt_msg}" "${yes}" "${no}"); then
          (
            wipefs -a -q /dev/"${part}"
            mkswap /dev/"${part}"
            swapon /dev/"${part}"
          ) &>/dev/null &
          pid=$! pri=0.1 msg="\n${swap_load} \n\n \Z1> \Z2mkswap /dev/${part}\Zn" load
        else
          swapon /dev/"${part}" &>/dev/null
          if [ "$?" -gt "0" ]; then
            msg "${swap_err_msg2}"
          fi
        fi
      else
        points=$(echo "${points}" | grep -v "${mnt}")

        if "${frmt}"; then
          if (yesno "${part_confirm_var}" "${write}" "${cancel}" 1); then
            (
              sgdisk --zap-all /dev/"${part}"
              wipefs -a /dev/"${part}"
            ) &>/dev/null &
            pid=$! pri=0.1 msg="\n${frmt_load} \n\n \Z1> \Z2wipefs -a /dev/${part}\Zn" load

            case "${FS}" in
            vfat)
              mkfs.vfat -F32 /dev/"${part}" &>/dev/null &
              ;;
            jfs | reiserfs)
              echo -e "y" | mkfs."${FS}" /dev/"${part}" &>/dev/null &
              ;;
            *)
              mkfs."${FS}" /dev/"${part}" &>/dev/null &
              ;;
            esac
            pid=$! pri=1 msg="\n${load_var1} \n\n \Z1> \Z2mkfs.${FS} /dev/${part}\Zn" load
          else
            part_menu
          fi
        fi

        (
          mkdir -p "${ARCH}"/"${mnt}"
          mount /dev/"${part}" "${ARCH}"/"${mnt}"
          echo "$?" >/tmp/ex_status.var
          sleep 0.5
        ) &>/dev/null &
        pid=$! pri=0.1 msg="\n${mnt_load} \n\n \Z1> \Z2mount /dev/${part} ${ARCH}${mnt}\Zn" load

        if [ "$(</tmp/ex_status.var)" -gt "0" ]; then
          msg "\n${part_err_msg2}"
        fi
      fi
    fi

    part_menu
  fi
}
